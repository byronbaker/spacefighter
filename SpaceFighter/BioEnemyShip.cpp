
#include "BioEnemyShip.h"


BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void BioEnemyShip::Update(const GameTime *pGameTime)
{
	if (IsActive())//Makes sure it's a objects that should be moving
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;//Makes X equal to Sin(Time*PI + Index of the Game Object)*Speed*Time*1.4
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());//Moves the object the the correct position

		if (!IsOnScreen()) Deactivate();// Makes the object inactive if it's no longer on screen
	}

	EnemyShip::Update(pGameTime);//Runs the enemy ship update function
}


void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
