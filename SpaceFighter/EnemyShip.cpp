
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0)//Is The Ship Delayed?
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();//Set the delay to the correct time

		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();//Activate if the delay is over
		}
	}

	if (IsActive())
	{
		m_activationSeconds += pGameTime->GetTimeElapsed();//Increase activation time based on time elapsed
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate(); //Deactivate if it's been active for more than 2 seconds and off screen
	}

	Ship::Update(pGameTime);//Run the ship update function
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}