
#include "Ship.h"


Ship::Ship()
{
	SetPosition(0, 0);
	SetCollisionRadius(10);//Sets a Default Size and Position

	m_speed = 300;
	m_maxHitPoints = 3;
	m_isInvulnurable = false;//Sets other Default Variables

	Initialize();
}

void Ship::Update(const GameTime *pGameTime)
{
	m_weaponIt = m_weapons.begin();//Iterates m_weapons
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Update(pGameTime);//Updates the ship weapons
	}

	GameObject::Update(pGameTime);//Runs the Game Object Update Funtion
}

void Ship::Hit(const float damage)
{
	if (!m_isInvulnurable)//Checks if Invulnurable
	{
		m_hitPoints -= damage;//Subtracts the damage form the health

		if (m_hitPoints <= 0)//Has no health left
		{
			GameObject::Deactivate();//Kills the ship
		}
	}
}

void Ship::Initialize()
{
	m_hitPoints = m_maxHitPoints;//Makes Current health equal to starting health
}

void Ship::FireWeapons(TriggerType type)
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Fire(type);
	}
}

void Ship::AttachWeapon(Weapon *pWeapon, Vector2 position)
{
	pWeapon->SetGameObject(this);
	pWeapon->SetOffset(position);
	m_weapons.push_back(pWeapon);
}